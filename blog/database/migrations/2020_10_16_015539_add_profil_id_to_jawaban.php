<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProfilIdToJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('komentar_jawaban', function (Blueprint $table) {
            //
            $table->unsignedBigInteger ('jawaban_pertanyaan');
            $table->unsignedBigInteger ('profil_id');
            $table->foreign ('jawaban_pertanyaan')->references ('id')->on('pertanyaan');
            $table->foreign ('profil_id')->references ('id')->on('profil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komentar_jawaban', function (Blueprint $table) {
            //
            $table->dropForeign (['profil_id', 'jawaban_pertanyaan']);
            $table->dropColumn (['profil_id', 'jawaban_pertanyaan']);
        });
    }
}
